const ga = require("golos-addons");
const global = ga.global;
const golos = ga.golos;

global.initApp("reposter");

const log = global.getLogger("reposter");

const CONFIG = global.CONFIG;



if (CONFIG.websocket) {
    golos.setWebsocket(CONFIG.websocket);
}

async function repost(reposter, comment) {

    const reposters = await golos.golos.api.getRebloggedByAsync(comment.author, comment.permlink);
    if (reposters.includes(reposter.account)) {
        log.debug("already reposted", comment.author, comment.permlink);
        return;
    }

    log.info("repost", comment.author, comment.permlink);
    await golos.golos.broadcast.customJsonAsync(reposter.key, [], [reposter.account],
        "follow",
        JSON.stringify([
            "reblog",
            {
                account: reposter.account,
                author: comment.author,
                permlink: comment.permlink
            }
        ])
    );
}

async function processComment(time, comment) {

    let content = await golos.getContent(comment.author, comment.permlink);

    if (!content) {
        log.warn("no content for", comment.author, comment.permlink);
        return;
    }

    log.debug("found comment", comment.author, comment.permlink, comment.parent_author, time, Date.parse(content.created));

    if (comment.parent_author != "" || Date.parse(comment.created) >= (time - 3000)) {
        log.debug("comment or edit")
        return;
    }

    const reposter = getReposter(comment.author);

    if (!reposter) {
        log.debug("no reposter defined for author", comment.author);
        return;
    }

    await repost(reposter, comment);
}

function getReposter(author) {
    return CONFIG.reposters[author];
}


async function processBlock(bn) {
    log.debug("processing block ", bn);

    //получаем все транзакции из блока
    let block = await golos.golos.api.getBlockAsync(bn);
    if (!block) {
        log.warn("skip block", bn);
        return false;
    }

    let time = Date.parse(block.timestamp);
    for (let tr of block.transactions) {
        for (let trop of tr.operations) {
            let op = trop[0];
            let opBody = trop[1];

            switch (op) {
                case "comment":
                    await processComment(time, opBody);
                    break;

            }
        }
    }
    return true;
}

const DELAY_BLOCKS = 3;

async function run() {
    let props = await golos.getCurrentServerTimeAndBlock();
    let block = props.block - DELAY_BLOCKS;
    log.info("Starting processing on block", block);
    while (true) {
        try {
            if (block > (props.block - DELAY_BLOCKS)) {
                await ga.global.sleep(1000 * 9); //check votes every 30 seconds
                props = await golos.getCurrentServerTimeAndBlock();
            } else {
                if (await processBlock(block)) {
                    block++;
                } else {
                    await ga.global.sleep(1000 * 3);
                }
            }
        } catch (e) {
            log.error("error in main loop", e);
            await ga.global.sleep(1000 * 9);
        }
    }
}

run();
